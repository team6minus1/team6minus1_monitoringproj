﻿using Microsoft.AspNetCore.Mvc;
using MonitoringProj2._3.Controllers;
using MonitoringProj2._3.Models.ViewModels;
using MonitoringProj2._3.Services;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using System.Threading.Tasks;

namespace MonitoringProj2._3.Testing
{

    public class CartItemControllerTest : Controller
    {
        APICartItemRepository cartItemRepository = new APICartItemRepository();

        public IActionResult TestIndex()
        {
            //https://localhost:44305/cartitemcontrollertest/testindex
            var controller = new CartItemController();

            if (controller.Index() != null)
            {
                return Content("index Page passed");
            }
            else
            {
                return Content("Index Page Failed test");
            }
        }

        public async Task<string> ItemStatsMathTest()
        {
            //https://localhost:44305/cartitemcontrollertest/itemstatsmathtest
            var model = await cartItemRepository.ReadAllAsync();

            List<ItemAverageVM> itemAverageVMs = new List<ItemAverageVM>();
            

            foreach (var j in model)
            {
                ItemAverageVM ItemAverageobj = new ItemAverageVM();
                ItemAverageobj.Item = j.Item;
                ItemAverageobj.CostList.Add(j.Cost);
                ItemAverageobj.DateTimeList.Add(j.Timestamp);
                ItemAverageobj.TotalSoldList.Add(j.Quantity);

                ItemAverageobj.LowPrice = ItemAverageobj.CostList.Min();
                ItemAverageobj.HighPrice = ItemAverageobj.CostList.Max();
                ItemAverageobj.Average = ItemAverageobj.CostList.Average();
                ItemAverageobj.Month = j.Timestamp.Month;
                ItemAverageobj.TotalSold = ItemAverageobj.TotalSoldList.Sum();
                if (j.Removed == true)
                {
                    ItemAverageobj.Removed++;
                }
                itemAverageVMs.Add(ItemAverageobj);
            }

            string builder = "";

            foreach (var i in itemAverageVMs)
            {
                builder += $"{i.Item} test results\n\n";

                if (i.LowPrice > 0)
                {
                    builder += $"Low price is greater than zero Test Passed\n";
                }
                else
                {
                    builder += $"Low price is less than zero ERROR\n";
                }

                if (i.HighPrice > 0)
                {
                    builder += $"High price is greater than zero Test Passed\n";
                }
                else
                {
                    builder += $"High price is less than zero ERROR\n";
                }

                if (i.Average > 0)
                {
                    builder += $"Average price is greater than zero Test Passed\n";
                }
                else
                {
                    builder += $"Average price is less than zero ERROR\n";
                }

                if (i.TotalSold >= 0)
                {
                    builder += $"Total Sold Passed\n";
                }
                else
                {
                    builder += $"Total Sold Test ERROR\n";
                }

                if (i.Removed <= 0)
                {
                    builder += $"Total Removed Test Passed\n\n";
                }
                else
                {
                    builder += $"Total Removed ERROR\n\n";
                }    
            }

            return builder;
        }


    }
}
